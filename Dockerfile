FROM rabbitmq:3
MAINTAINER tareqmohammadyousuf "tareq.y@gmail.com"

RUN rabbitmq-plugins enable rabbitmq_stomp
RUN rabbitmq-plugins enable rabbitmq_management
